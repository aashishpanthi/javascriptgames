const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");
const RocketImage = document.querySelector("#rocketIcon");
const scoreBox = document.querySelector('#score')

//Making canvas full width
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const canvasHeight = canvas.height;
const canvasWidth = canvas.width;

//Initializing the rocket
const rocket = {
  x: canvasWidth/2,
  y: canvasHeight/2,
  width: 30,
  height: 30
};

//Making an array of obstacles
const obstacles = [
  {
    x: 100,
    y: 100,
    width: 80,
    height: 80,
    direction: "up"
  },
  {
    x: canvasWidth/2 + canvasWidth/4,
    y: 300,
    width: 50,
    height: 70,
    direction: "left"
  }
];

//other essentials
let obstacleSpeed = 1;
const obstacleDirection = [
  "up",
  "down",
  "left",
  "right",
  "up_right",
  "up_left",
  "down_left",
  "down_right"
];
let obstacleFormationDuration = 2500
const originX = [-100, canvasWidth-100]
const originY = [-100, canvasHeight-100]
let score = 0;

window.onload = function () {
  main();
};

const main = () => {
  ctx.beginPath();
  ctx.clearRect(0, 0, canvasWidth, canvasHeight);
  makeObstacles();
  ctx.drawImage(RocketImage, rocket.x, rocket.y, rocket.width, rocket.height);
  ctx.closePath();
};

const makeObstacles = () => {
  obstacles.forEach((obstacle) => {
    ctx.rect(obstacle.x, obstacle.y, obstacle.width, obstacle.height);
    // ctx.arc(obstacle.x, obstacle.y, obstacle.width / 2,0,0);
    ctx.strokeStyle = "white";
    ctx.stroke();
    ctx.fillStyle = "#073648";
    ctx.fill();
  });

};

const moveObstacles = () => {
  obstacles.forEach((obstacle) => {
    switch (obstacle.direction) {
      case "up":
        obstacle.y -= obstacleSpeed;
        break;
      case "down":
        obstacle.y += obstacleSpeed;
        break;
      case "left":
        obstacle.x -= obstacleSpeed;
        break;
      case "right":
        obstacle.x += obstacleSpeed;
        break;
      case "up_right":
        obstacle.y -= obstacleSpeed;
        obstacle.x += obstacleSpeed;
        break;
      case "down_left":
        obstacle.y += obstacleSpeed;
        obstacle.x -= obstacleSpeed;
        break;
      case "up_left":
        obstacle.y -= obstacleSpeed;
        obstacle.x -= obstacleSpeed;
        break;
      case "down_right":
        obstacle.y += obstacleSpeed;
        obstacle.x += obstacleSpeed;
        break;
    }
    main();
  });
};

const moveRocket = (keycode) => {
  switch (keycode) {
    case 38:
      //uparrow
      rocket.y -= 10;
      main();
      break;

    case 40:
      //down arrow
      rocket.y += 10;
      main();
      break;

    case 37:
      //left arrow
      rocket.x -= 10;
      main();
      break;

    case 39:
      //right arrow
      rocket.x += 10;
      main();
      break;

    default:
    //do nothing
  }
};

const returnObstacle = () => {
  obstacles.forEach((obstacle) => {
    if (obstacle.x > canvasWidth + 200) {
      obstacle.direction = "left";
    } else if (obstacle.x < -200) {
      obstacle.direction = "right";
    } else if (obstacle.y > canvasHeight + 200) {
      obstacle.direction = "up";
    } else if (obstacle.y < -200) {
      obstacle.direction = "down";
    }
  });
};

const ckeckCollision = () =>{
    obstacles.forEach(obstacle=>{
        // if(rocket.x == obstacle.x && rocket.y == obstacle.y){
        //     alert('yes')
        // }
        if(rocket.x >= obstacle.x && rocket.x <= obstacle.x+obstacle.width && rocket.y >= obstacle.y && rocket.y <= obstacle.y+obstacle.height){
            alert('game over')
        }
    })
}

document.addEventListener("keydown", (e) => moveRocket(e.keyCode));

setInterval(() => {
  obstacles.push({
    x: originX[Math.floor(Math.random() * 2)],
    y: originY[Math.floor(Math.random() * 2)],
    width: Math.random() * (80 - 10) + 10,
    height: Math.random() * (80 - 10) + 10,
    direction: obstacleDirection[Math.floor(Math.random() * (obstacleDirection.length - 1))]
  });
  main();
}, obstacleFormationDuration);

setInterval(()=>{
    returnObstacle();
    moveObstacles();
    ckeckCollision()
    main();
},50)

setInterval(() => {
    obstacleSpeed += 0.5
    obstacleFormationDuration -=100
}, 5000);


setInterval(()=>{
  score++
  scoreBox.innerHTML = score;
},1000)